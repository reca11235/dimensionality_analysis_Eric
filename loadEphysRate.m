function d = loadEphysRate(areas_selected, sessions_selected, stimuli_selected, flag_discrete)
%%
file = 'data.mat';
data = load(file);

d = {};
d_vars = cellstr({'spk', 'session', 'area', 'unit', 'trial', 'stimulus', 'start', 'stop', 'stimid'});

if isempty(areas_selected)
    areas_selected = {'V1','LGN'};
end

if isempty(stimuli_selected)
    stimuli_selected = { 'none_before', 'none_after', 'drifting_gratings', 'natural_images','natural_movie_Anton_1', 'locally_sparse_noise','natural_movie_Anton_3', 'natural_movie_Anton_2', 'natural_movie_3', 'flash_50ms', 'static_gratings', 'natural_movie_1', 'flash_250ms'};
end

if isempty(sessions_selected)
    sessions_selected = fieldnames(data);
end

progress = 0;
row_d = 0;
for i_ses = 1:numel(sessions_selected)
    i_ses
    ses = sessions_selected{i_ses};        
        areas = fieldnames(data.(ses).processing);
        stimuli = stimuli_selected;
        for i_area = 1:numel(areas)
            area = areas{i_area};
            if any(strcmp(area, areas_selected))
                units_array = data.(ses).processing.(area).UnitTimes.unit_list;
                units = arrayfun(@(x) ['p' num2str(str2num(units_array(x,:)))], 1:size(units_array,1),'uni',0);
                for i_unit = 1:numel(units)
                    unit = units{i_unit};
                    spk = data.(ses).processing.(area).UnitTimes.(unit).times;
                    for i_stim = 1:numel(stimuli)
                        stimulus = stimuli{i_stim};                        
                            if flag_discrete
                                N_trials = data.(ses).stimulus.presentation.(stimulus).num_samples;                                                                                                
                            else                                
                                N_trials = 1;                                
                            end                                                                                                                
                            for i_trial = 1:N_trials                                                                
                                switch stimulus
                                    case {'none_before'}                                        
                                        start = 0;
                                        stop = data.(ses).stimulus.presentation.drifting_gratings.timestamps(1,1);
                                        stimid = nan;
                                    case {'none_after'}
                                        start = data.(ses).stimulus.presentation.locally_sparse_noise.timestamps(end,2);
                                        stop = max(spk);
                                        stimid = nan;
                                    otherwise                     
                                        if flag_discrete
                                            start = data.(ses).stimulus.presentation.(stimulus).timestamps(i_trial,1);
                                            stop = data.(ses).stimulus.presentation.(stimulus).timestamps(i_trial,2);
                                            stimid = data.(ses).stimulus.presentation.(stimulus).timestamps(i_trial,3:end);                                
                                        else
                                            start = data.(ses).stimulus.presentation.(stimulus).timestamps(1:end,1);
                                            stop = data.(ses).stimulus.presentation.(stimulus).timestamps(1:end,2);
                                            stimid = data.(ses).stimulus.presentation.(stimulus).timestamps(1:end,3:end);                                
                                        end
                                end                                                                
                                row_d = row_d + 1;
                                d{row_d,1} = spk(and(spk > start(1), spk < stop(end))) - start(1);
                                d{row_d,2} = ses;
                                d{row_d,3} = area;
                                d{row_d,4} = unit;
                                d{row_d,5} = i_trial;
                                d{row_d,6} = stimulus;                                                                                         
                                d{row_d,7} = start;
                                d{row_d,8} = stop-start;
                                d{row_d,9} = stimid;                                                                                                       
                            end                                                         
                        progress_ = sub2ind([numel(stimuli_selected),numel(units),numel(sessions_selected)], i_stim, i_unit, i_ses)/(numel(units) * numel(stimuli_selected) * numel(sessions_selected)) * 100;
                        if ceil(progress_/10) - ceil(progress/10) > 0
                            progress = progress_;
                            fprintf('%d  ', round(progress));
                        end
                    end
                end
            end
        end    
end

% %% GENERATING TABLE
d = cell2table(d, 'variablenames', d_vars);
d.area = categorical(d.area);
d.session = categorical(d.session);
d.trial = categorical(d.trial);
d.unit = categorical(d.unit);
d.stimulus = categorical(d.stimulus);
