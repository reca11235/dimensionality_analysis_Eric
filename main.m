%% ANALYSIS OF EPHYS PRELIMINARY DATASET


%%  Handy functions
vertvec = @(x) reshape(x, [numel(x),1]);
vertcatcell = @(x) vertcat(x{:});
horzcatcell = @(x) horzcat(x{:});
Cov = @(x) (x'*x)/size(x,1);
tr2C = @(Cov) sqrt(trace(Cov^2)/size(Cov,1));
dimC = @(Cov) (trace(Cov)^2) / trace((Cov)^2) / size(Cov,1);
mnC = @(Cov) mean(Cov(:));
spk2bin = @(x, binsize) vertcatcell(vertvec(arrayfun(@(i_x) histcounts(x{i_x}, 0:binsize:binsize*ceil(max([x{:}])/binsize))/binsize, 1:numel(x), 'uni', 0)));
spk2trial = @(x, trials_time) vertcatcell(vertvec(arrayfun(@(i_x) histcounts(x{i_x}, trials_time), 1:numel(x), 'uni', 0)));


%% Data selection (leaving the field empty loads all the data on that
% condition)
areas_selected = {'LGN', 'V1'};
stimuli_selected = {'drifting_gratings', 'natural_images','static_gratings', 'locally_sparse_noise' };%'natural_movie_Anton_1', 'locally_sparse_noise','natural_movie_Anton_3', 'natural_movie_Anton_2', 'natural_movie_3', 'flash_50ms', 'static_gratings', 'natural_movie_1', 'flash_250ms'};
sessions_selected = {}; 
flag_discrete = 0; %setting this to 1 loads the data trial by trial and it is not desirable. Trials will be separated later on during binning


%% Data loading
d = loadEphysRate(areas_selected, sessions_selected, stimuli_selected, flag_discrete);

% This creates the mapping between the stimulus and the parametrization of that stimulus
stimuli_all = { 'none_before', 'none_after', 'drifting_gratings', 'static_gratings', 'natural_images','natural_movie_Anton_1', 'locally_sparse_noise','natural_movie_Anton_3', 'natural_movie_Anton_2', 'natural_movie_3', 'flash_50ms', 'natural_movie_1', 'flash_250ms'};
stimuli_ids = {'frame', 'frame', {'time_fr', 'orientation'}, {'spat_fr', 'orientation', 'phase'}, 'frame', 'frame', 'frame', 'frame', 'frame', 'frame', 'color', 'frame', 'color'};
stim2ids = table(stimuli_all', stimuli_ids(:), 'variablename', {'stimulus', 'ids'});


%% Group neurons per session and compute firing rates in each stimulus presentation

% Group variables and compute time variables and spikes
[G_stimses, G_stim, G_ses] = findgroups(d.stimulus, d.session);
spk_sesstim = varfun(@(x) x, d, 'InputVariables', 'spk', 'GroupingVariables', {'stimulus', 'session'}, 'OutputFormat','cell');
bins_sesstop = varfun(@(x) x, d, 'InputVariables', 'stop', 'GroupingVariables', {'stimulus', 'session'}, 'OutputFormat','cell');
bins_sesstart = varfun(@(x) x, d, 'InputVariables', 'start', 'GroupingVariables', {'stimulus', 'session'}, 'OutputFormat','cell');

% create the bins to compute rates
% bins_sesall = cellfun(@(x,y) sort([vertvec(x{1}-x{1}(1));vertvec(x{1}+y{1}-x{1}(1))]'), bins_sesstart, bins_sesstop, 'uni', 0);
bins_sesall = cellfun(@(x,y) sort([vertvec(x{1}-x{1}(1));vertvec(x{1}+0.250-x{1}(1))]'), bins_sesstart, bins_sesstop, 'uni', 0);
rates_all = cellfun(@(x,y) spk2trial(x, y), spk_sesstim, bins_sesall, 'uni', 0);
rates = cellfun(@(x) x(:,1:2:end), rates_all, 'uni', 0);

% create stimulus id (parameters of the stimulus)
stimid_sesstim = varfun(@(x) x, d, 'InputVariables', 'stimid', 'GroupingVariables', {'stimulus', 'session'}, 'OutputFormat','cell');
stimid_sesstim = cellfun(@(x) x{1}, stimid_sesstim, 'uni', 0);

% put together the data table
dr = table(G_stim, G_ses, 'VariableNames', {'stimulus', 'session'});
dr.rates = rates;
dr.stimid = stimid_sesstim;

% Merge and rename some stimuli of the same kind (useless if these stimuli are not considered)
dr.stimulus = mergecats(dr.stimulus,{'natural_movie_Anton_1', 'natural_movie_Anton_2','natural_movie_Anton_3', 'natural_movie_1', 'natural_movie_3', 'movie_anton', 'movie_natural'},'movies');
dr.stimulus = mergecats(dr.stimulus,{'flash_250ms', 'flash_50ms'},'flashes');
dr.stimulus = mergecats(dr.stimulus,{'none_before', 'none_after'}, 'no stimuli');
[G_stimses, L_stim, L_ses] = findgroups(dr.stimulus, dr.session);
rates = vertvec(arrayfun(@(i_x) [dr.rates{G_stimses==i_x}] , unique(G_stimses),  'uni', 0));
stimid = vertvec(arrayfun(@(i_x) [dr.stimid{G_stimses==i_x}] , unique(G_stimses),  'uni', 0));
dr = table(L_stim, L_ses, 'VariableNames', {'stimulus', 'session'});
dr.rates = rates;
dr.stimid = stimid;


%% Analysis of Correlation dimensionality across stims

% Z =generate_data('swiss', 3000, 0.9);

sessions = cellstr(unique(dr.session));
for i_ses = 1:numel(sessions)
    clf
for i_stim = 1:numel(stimuli_selected)
    stimulus = stimuli_selected{i_stim};    
    session = sessions{i_ses};
    Z = dr{(dr.stimulus==stimulus).*(dr.session==session)>0,3}{1}';
    
    Zp = pcadrt(Z,20); 
%     Zp = pca(Z');
%     Zp = Zp(:,1:20);
    
    X=Zp(1:min(1000,size(Zp,1)),:);%(:,[1,2]);
    neps = 200;
    n = size(X, 1);
    X = X';
    XX = sum(X .^ 2);
    onez = ones(1,n);
    DD = bsxfun(@plus, XX', bsxfun(@plus, XX, -2 * (X' * X)));
    DD = DD - diag(diag(DD));
    % r1 = min(DD(DD>0)); r2 = median(DD(:));
    r1 = 2; r2 = 100;
    rs = logspace(log10(r1), log10(r2), neps);
    s1 = 0; s2 = 0; stot = 0;
    dist = sqrt(triu(DD)); dist = dist(dist>0);
    distmrs = bsxfun(@minus,repmat(dist, 1, neps), rs);
    Crs = mean(distmrs<0,1);
    lCrs = log(Crs(1:2:end));
    lrs = log(rs(1:2:end));
    rss = (rs(1:2:end));
    dims_all = diff((lCrs)) ./ diff((lrs));
        
    subplot(221);    
    hold on;
    plot(log(rss), lCrs,'.','linewidth',2)
    title('CorrDim cumulative plot');
    xlabel('log(radius)')
    ylabel('Integral CorrDim')    
    
    subplot(222);
    title('CorrDim');
    hold on;
    plot(log(rss(1:end-1)), dims_all)
    xlim([log(r1),log(r2)]);
    xlabel('log(radius)')
    ylabel('CorrDim')
    
    subplot(223);    
    hold on;
    plot(Z(:,1:end-1), Z(:,2:end), '.');
    title('firing rates btw neurons');    
    xlabel('neuron 1');
    ylabel('neuron 2');
    
    subplot(224);
    hold on;
    plot(Zp(:,1), Zp(:,2), '.');    
    title('PC1-2');
    xlabel('PC 1');
    ylabel('PC 2');
       
end
suptitle(['session ', session])
legend(cellstr(stimuli_selected));
drawnow();
pause();
end

%% Compute MLE dimensionality

% Set neighborhood range to search in
k1 = 3;
k2 = 100;
r1 = 10; r2 = 100;
rs = logspace(log10(r1), log10(r2), neps);

sessions = cellstr(unique(dr.session));
for i_ses = 1:numel(sessions)
    clf
for i_stim = 1:numel(stimuli_selected)
    stimulus = stimuli_selected{i_stim};    
    session = sessions{i_ses};
    Z = dr{(dr.stimulus==stimulus).*(dr.session==session)>0,3}{1}';
    
    Zp = pca(Z,20);
    X=Zp(1:min(1000,size(Zp,1)),:)';%(:,[1,2]);

    [d n] = size(X);
    X2 = sum(X.^2, 1);
    knnmatrix = zeros(k2, n);
    distance = repmat(X2, n, 1) + repmat(X2', 1, n) - 2 * X' * X;
    distance = sort(distance);
    knnmatrix= .5 * log(distance(2:k2 + 1,:));
    
    % Compute the ML estimate
    S = cumsum(knnmatrix, 1);
    indexk = repmat((k1:k2)', 1, n);
    dhat = -(indexk - 2) ./ (S(k1:k2,:) - knnmatrix(k1:k2,:) .* indexk);
    
    % Average over estimates and over values of k
    dims_all = arrayfun(@(y) mean(mean(dhat(distance(k1:k2,:)<y))), rs);
    
%     hist(dhat(:),1:50);xlim([0,50]);    
    hold on;
    plot(rs, dims_all,'.','linewidth',2)    
end

legend(cellstr(stimuli_selected));
drawnow();
pause();
end

%% Analysis of MiND_ML dimension
clf
L2 = @(a,b) real(sqrt(bsxfun(@plus, sum(a .* a)', bsxfun(@minus, sum(b .* b), 2 * a' * b))));
% The log likelihood derivative squared:
fun = @(r,d,k)((n/d)+sum(log(r)-((k-1).*(log(r).*r.^d))./(1-r.^d)))^2;
k = 100;
ks = 3:1:100;
sessions = cellstr(unique(dr.session));
for i_ses = 1:numel(sessions)
    clf
for i_stim = 1:numel(stimuli_selected)
    stimulus = stimuli_selected{i_stim};    
    session = sessions{i_ses};
    Z = dr{(dr.stimulus==stimulus).*(dr.session==session)>0,3}{1}';
    
    Zp = pca(Z,20);
    X=Zp(1:min(1000,size(Zp,1)),:)';
    
    dists = L2(X, X);
    [dists, inds] = sort(dists, 2);
    inds = inds(:,2:k-1);
    dists = dists(:,2:k);
    dists = dists(:,1:end-1)./repmat(dists(:,end),[1,size(dists,2)-1]);
    r1 = dists(:,1);
    funr = @(d,k) fun(r1,d,k);
    opts = optimset('Display','off','TolX',1e-3);
    dims_all = arrayfun(@(k) fminsearch(@(d) funr(d,k), d,opts), ks);
         % Evaluating it:
    vals = zeros(1,1000);
%     for d=1:1000; vals(d)=funr(d,100); end
    semilogy(ks, dims_all,'linewidth',2);
    hold on;
end

legend(cellstr(stimuli_selected));
drawnow();
pause();
end
%%

% estimators = {'dimCov', 'MiND_ML','MLE', 'DANCoFit', 'CorrDim', 'GMST'};%,'MiND_KL'};%, 'MiND_ML',,'MiND_KL', 'DANCo'
% for i_est = 1:numel(estimators)
%     tic;
%     try idEst(i_est) = eval([estimators{i_est}, '(rep)']);  catch idEst(i_est)=nan; end
%     spentTime(i_est) = toc;
%     dims(i_sigma,i_est, i_T, i_N) = idEst(i_est);
% end
% 
% Nel_sub = @(G_sub) ceil(min(arrayfun(@(i_x) sum(G_sub==i_x), unique(G_sub)))/2);
% Nel_subcell = @(x,G_sub) floor(min(arrayfun(@(i_x) numel(vertcatcell(x(G_sub==i_x))), unique(G_sub)))/2);
% bootdist = @(x, fun, G_sub, N_boot) arrayfun(@(i_boot) fun(vertcatcell(arrayfun(@(i_Gsub) datasample(x(G_sub==i_Gsub), Nel_sub(G_sub)), unique(G_sub),'uni',0))), 1:N_boot);
% bootdistcell = @(x, fun, G_sub, N_boot) arrayfun(@(i_boot) fun(vertcatcell(arrayfun(@(i_Gsub) datasample(vertcatcell(x(G_sub==i_Gsub)), Nel_subcell(x,G_sub)), unique(G_sub),'uni',0))), 1:N_boot);
% 
% b = splitapply(@(x,y) prctile(bootdist(x, @mean, findgroups(y), 1000),1:100), dht.FirstTone_N, dht.trigger, G);
% 
% 
% 




