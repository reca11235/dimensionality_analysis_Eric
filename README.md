## This repository contains the processing of the preliminary Ephys dataset

The code heavily relies on the "table" data structure of matlab. Although it is commented it is recommended to the user to familiarize with this data structure if he/she is not.

The loading function is loadEphysRate but this shall be used as shown in main.
