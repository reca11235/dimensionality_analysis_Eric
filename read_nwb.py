import numpy as np
import pandas as pd
import os
import glob

import h5py
import numpy as np
import numpy.linalg as lng
import scipy as scy
import scipy.stats as sts
import matplotlib.pyplot as plt
import os
import matplotlib
from collections import OrderedDict
import networkx as nx
import scipy.io as sio


##
def print_dict(d):
    new = {}
    for k, v in d.items():
        if hasattr(v, 'items'):
            v = print_dict(v)
        if hasattr(v, 'value'):
            vv = v.value
        else:
            vv = v
        if isinstance(k, (bytes, bytearray)):
            new['p'+k.decode("utf-8")] = vv
        else:
            if k.isdigit():
                new['p'+k] = vv
            else:
                new[k] = vv
    return new


class EphysData(object):
    def __init__(self, expt_dir):

        nwb_files = glob.glob(os.path.join(expt_dir, '*.nwb'))
        if len(nwb_files)>1:
            print("Warning:  more than 1 *.nwb file found; using the first")
            

        self.filepath = nwb_files[0]
        self.parent_dir = os.path.dirname(expt_dir)

        self.data = h5py.File(self.filepath, 'r')

        self.areas = self.data['processing'].keys()

        get_spikes_times = lambda area, unit_id:  self.data['processing'][area]['UnitTimes'][unit_id]['times'].value

        spike_times_dict = {}
        for area in self.areas:
            spike_times_dict[area] = {}

            units = self.data['processing'][area]['UnitTimes']['unit_list']
            for unit_id in units:
                spike_times = get_spikes_times(area, unit_id)
                spike_times_dict[area][unit_id] = spike_times

        self.spike_times_dict = spike_times_dict

        self.stim_types = self.data['stimulus/presentation'].keys()

    def get_spikes_times(self, area, unit_id):
        return self.spike_times_dict[area][unit_id]

    def get_stim_table(self, stimulus):
        return pd.DataFrame(self.data['stimulus/presentation/'+stimulus+'/timestamps'].value, columns=['Start', 'End', 'Frame'])

    def get_stim_template(self, stimulus):
        template_file = os.path.join(self.parent_dir, 'ns_stimulus')+'.npy'
        return np.load(template_file)

    def compute_spike_counts_per_trial(self, stimulus, offset=0.03):
        """offset is in seconds; offset accounts for the delay between retina and visual cortex"""

        spike_count_dict = {}
        unit_id_map = {}

        stim_table = self.get_stim_table(stimulus=stimulus)

        num_trials = stim_table.shape[0]
        

        for area in self.areas:

            num_neurons = len(self.spike_times_dict[area])
            spike_count_dict[area] = np.zeros([num_trials, num_neurons])
            unit_id_map[area] = {}

            for i, unit_id in enumerate(self.spike_times_dict[area]):

                unit_id_map[area][i] = unit_id
                spike_times = self.spike_times_dict[area][unit_id]

                trial = 0
                start, end = stim_table.iloc[0][:2]
                start+=offset
                end+=offset

                for time in spike_times:
                    while time >= end and trial<num_trials:
                        trial = trial + 1
                        if trial<num_trials:
                            start, end = stim_table.iloc[trial][:2]  # won't work for other stimulus types necessarily
                            start += offset
                            end += offset
                    if trial>=num_trials:
                        break
                    
                    if time >= start:
                        spike_count_dict[area][trial, i] =  spike_count_dict[area][trial, i]+1
                    
        return spike_count_dict, unit_id_map

##
data = dict()
sev_dir = 'NWBfiles_7_14_17/'
files = os.listdir(sev_dir)
for i_file, file in enumerate(files):
    dir_file = sev_dir + file
    nwb_files = glob.glob(os.path.join(dir_file, '*.nwb'))
    if len(nwb_files)>1:
            print("Warning:  more than 1 *.nwb file found; using the first")
    if len(nwb_files)>0:
        ed = EphysData(sev_dir+file)
        data[file] = ed.data

nd = print_dict(data)
# sio.savemat('data.mat', nd)


##
stime_table = ed.get_stim_table('natural_images')
stime_template = ed.get_stim_template('natural_images')
units = ed.data['processing']['V1']['UnitTimes']['unit_list']
spikes_table = ed.get_spikes_times('V1', (b'151', b'150'))
spike_counts, unit_ids = ed.compute_spike_counts_per_trial('natural_images',0)


